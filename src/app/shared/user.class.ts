export class User {
    email: string;
    password: string;
    nombre: string;
    apellidos: string;
    genero: string;
    ubicacion: string;
    favoritos: string[] = [];
    publicaciones: string[] = [];
}