import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { LoginPage } from 'src/app/login/login.page';
import { UserI } from 'src/app/interfaces/user';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  posts: any;
//user = LoginPage.CurrentUser;
  user: UserI;
  constructor(
    private loadingCtrl: LoadingController,
    private toastCtlr: ToastController,
    private firestore: AngularFirestore ) {
      this.user = LoginPage.CurrentUser;
     // console.log(this.user);
      
    }

  ionViewWillEnter() {
    //this.getPosts();
  }

  /*Obtener los información de la base de datos
  async getPosts() {
    let loader = this.loadingCtrl.create({
      message: "Please wait..."
    });

    //(await loader).present();

    try {
      this.user = LoginPage.CurrentUser;

      this.firestore.collection("users")
      .snapshotChanges()
      .subscribe(data => {
        this.posts = data.map(e => {
          return {
            //id: e.payload.doc.id,
            nombre: e.payload.doc.data()["nombre"],
            apellidos: e.payload.doc.data()["apellidos"],
            email: e.payload.doc.data()["email"]
          };
        });
      });

    } catch (e) {
      this.showToast(e);
    }
  }

  showToast(message: string) {
    this.toastCtlr
      .create({
        message: message,
        duration: 3000
      })
      .then(toastData => toastData.present());
  }


  //Editar datos



  */
  ngOnInit() {
  }

}
