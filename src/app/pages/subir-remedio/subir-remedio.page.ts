import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Tip } from '../../interfaces/tip';
import { ToastController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-subir-remedio',
  templateUrl: './subir-remedio.page.html',
  styleUrls: ['./subir-remedio.page.scss'],
})
export class SubirRemedioPage implements OnInit {

  post = {} as Tip;

  constructor(
    private toastCtrl: ToastController, 
    private loadingCrtl: LoadingController,
    private firestore: AngularFirestore
    ){}


  ngOnInit() {
  }

  async createPost(post: Tip){

    if(this.formValidation()){
      let loader = this.loadingCrtl.create({
        message: "Por favor, espere..."
      });
      (await loader).present();
      try {
        post.likes = 0;
        await this.firestore.collection("tips").add(post);
      } catch(e){
        this.showToast(e);
      }

      (await loader).dismiss();

    }

  }

  formValidation(){
    if(!this.post.titulo){
      this.showToast("Ingresa el nombre");
      return false;
    }

    if(!this.post.categoria){
      this.showToast("Ingresa la categoria");
      return false;
    }

    if(!this.post.titulo){
      this.showToast("Ingresa la receta");
      return false;
    }

    if(!this.post.urlToImage){
      this.showToast("Adjunte la url de la imagen relacionada con la receta");
      return false;
    }

    return true;
  }

  showToast(message: string){
    this.toastCtrl.create({
      message: message,
      duration: 3000
    })
    .then(toastData => toastData.present());
  }

}
