import { Component, OnInit } from '@angular/core';
import { TipsService } from '../../services/tips.service';
import { Observable } from 'rxjs';
import { Tip } from '../../interfaces/tip';
import { UserI } from '../../interfaces/user';
import { LoginPage } from 'src/app/login/login.page';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

  items: Tip[] = [];

  constructor(public tipsService: TipsService) {
    
      this.tipsService.list().subscribe((resp: any) => {
        this.items = resp;
      });  
    
    
  }
  ngOnInit()
  {
    
  }
}
