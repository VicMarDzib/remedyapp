export interface UserI {
      id: any,
      nombre: string,
      apellidos: string,
      email: string,
      clave: string,
      genero: string,
      ubicacion: string,
      favoritos: string[],
      publicaciones: string[]
}
